###############
Getting started
###############

Prevision.io is an automated SaaS machine learning platform that enables you to create and deploy
powerful predictive models and business applications in one-click.

Prevision.io has distinguish 2 environments :

- The ``Studio``, in which you can create predictive model powered by automated machine learning
- The ``Store``, in which you can deploy ressources (such as model, dashboard or notebooks) directly to your business users

If you have any question or remark, you can reach us through the contact form accessible in the user menu (see below).

**********
Connection
**********

In order to connect to Prevision.io, you need to open a recent browser (Chrome or Firefox) to the following address:
https://xxx.prevision.io [xxx is the subdomain related to your company]

If you are not already logged in, you'll land to the following screen:

.. image:: _static/images/connection.png
   :align: center

Connection is done with email address and password.
If you have forgotten your password, you can reset it by clicking on the "Forgotten password" link.

**********
Navigation
**********

Top navigation bar
==================

Once connected, navigation is done thanks to a bar located at the top of your screen.

.. image:: _static/images/top_bar.png
   :align: center


User menu
=========

.. image:: _static/images/user_menu.png
   :align: center

At the top right of the screen, the user menu allows you to:

* Switch displayed language between :

    * English
    * French

* Access users settings
* Access the API key configuration and the admin screen (if you have the admin rights)
* Access the scheduler
* Access the documentation
* Access the support and contact page
* View the current version number
* Logout
