.. doctest documentation master file, created by
   sphinx-quickstart on Tue Nov 28 16:07:35 2017.

##############################
Prevision.io --- Documentation
##############################

.. toctree::
   :maxdepth: 2
   :caption: Reference documentation

   getting_started
   use_cases
   data
   new_usecase
   versioning
   dashboard
   projects_supervised
   projects_timeseries
   projects_object_detector
   notebooks
   scheduler
   store
   api
   account
   how
   release_notes
   known_issues