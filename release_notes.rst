#############
Release notes
#############


*************
Version 10.13
*************

Release date: 2020-10-22

Studio
------

Features
********

- User interface of models selection : Users choices regarding model selection in advanced options during the creation of a use case are now extended.
- FastText : First NLP treatments are coming in Prevision.io platform. Text roles features of a use case can now be optimized using NLP algorithm
- Image Detector use cases : migration to YoloV5 treatment allowing a better treatment for image detector use cases
- UX/UI improvements


Bug fixes
*********

- Fix of a display issue regarding number format depending on user interface language
- Lack of some calculated feature grades
- F1 score calculation of a model is now based on the optimal threshold value
- Fix of graphical display regarding bivariate analysis

Store
-----

Features
********

- Webapp and notebook versioning : creation of new version of an already existing web app and notebook
- Access to private GitLab repo : SSO connexion to GitLab
- Build and deploy logs are now available for webapps
- application list of allowed user is now editable


Bug fixes
*********

- Some field were not typed producing errors when fulfill with wrong type of data
- fix regarding an issue affecting all user of an instance instead of the user selected

*************
Version 10.12
*************

Release date: 2020-10-08

* Feature engineering importance
* Deployed Model SDK


*************
Version 10.10
*************

Release date: 2020-09-11

* Free trial version
* New interface for deployed usecases

************
Version 10.9
************

Release date: 2020-08-20

* New Prevision.io Studio homepage
* New Prevision.io Studio help page


************
Version 10.8
************

Release date: 2020-08-06

* SDK: Add versioning & sharing methods
* Add embedded support in store and studio through Zendesk

************
Version 10.7
************

Release date: 2020-07-23

* Connectors for Google Cloud Platform Buckets and BigQuery
* Advanced analytics for time series datasets
* Public mode for app deployment in store
* Subdomain URL mode for app deployment in store
* Add the capability to define environment variables when deploying apps in stores


************
Version 10.6
************

Release date: 2020-07-09

* Various improvements for apps deployment in store
* Better handling of very large datasets (> 10k columns)


************
Version 10.5
************

Release date: 2020-06-25

* Optimizations & bug fixes
* Model and app deployment is now entirely located in the Prevision.io store

************
Version 10.4
************

Release date: 2020-06-11

* Detailed statistics and analyses for datasets accessible from the data page

************
Version 10.3
************

Release date: 2020-05-28

* Usecase versioning


************
Version 10.1
************

Release date: 2020-04-10

* Create a new usecase from an existing one
* Simple models updated in order to match classical model analytics
* R & Python packages updated + new packages availlable for development environment

************
Version 10.0
************

Release date: 2020-03-05

* New graph-based usecase training monitoring
* Update scheduler page

***********
Version 9.7
***********

Release date: 2020-02-20

* Update notebook page to include current CPU & RAM usage
* Update and relocate administration page (now in top-right menu)
* Access data explorer from data screen

***********
Version 9.6
***********

Release date: 2020-02-06

* Change and relocate main menu to top bar
* Start usecase from data screen
* Update contact page

***********
Version 9.5
***********

Release date: 2019-12-19

* Refactoring of the main dashboard screen
* Refactoring of the usecase screen, including new analytics
* R & Python packages updated to matchs usecase APIs
* Improved explain screen stability when simulating predictions
* Added support of object detection usecase with CPU only (might take some computing time)
* Feature quality estimation

***********
Version 9.4
***********

Release date: 2019-10-04

* Refactoring of the data screen
* R & Python packages updated to matchs data screen APIs
* Improved rules of detection of typical columns (ID, TARGET, FOLD, WEIGHT)
* Improved explain screen stability when values are missing
* Improved date columns parsing in a dataset that handles multiple time zones
* Faster prediction time retrival when listing a high number of predictions
* Creation of an open data base with accessible data for special days (holidays, public days, sales, ...) and for weather data

***********
Version 9.3
***********

Release date: 2019-08-14

* Refactoring of the new use case screen
* R & Python packages updated to matchs new use case APIs
* Refactoring of Prevision.io APIs. Documentation available @ https://xxx.prevision.io/api/documentation (xxx = instance name)
* Creation of instance specific Prevision.io's store, visible @ https://xxx.prevision.io/store (xxx = instance name)
* Optimisation of training time for gradient boosting trees models