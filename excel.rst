﻿############
Add in EXCEL
############

Prevision.io offre la possibilite d'utiliser sa plateforme directement dans le logiciel Excel.

************
Installation
************

Pour cela, vous devrais télécharger l'add-in directement sur le Store de complement du logiciel. Pour y accéder,

- Sur Excel: rendez-vous dans l'onglet "Insertion", puis dans le bloc "Compléments" cliquer sur "Télécharger des compléments" et cherchez l'add-in

.. image:: _static/images/excel_chemin.png
   :align: center

- Sur Excel-Online: rendez-vous dans l'onglet "Insertion", puis dans le bloc "Compléments" cliquer sur "Compléments d'Office", choisissez l'onglet "Store" et cherchez l'add-in

.. image:: _static/images/excel_chemin_online.png
   :align: center

L'obtention de l'add-in est payant et est conditionné à l'utilisation d'une license Entreprise de Prevision.io pour pouvoir l'utiliser. 

Vous pouvez maintenant ouvrir l'add-in, pour cela vous devez ouvrir l'onglet "Acceuil" et aller sur le bloc "Prevision.io Add-in" et cliquer sur l'icone pour ouvrir l'interface. Il vous reste une dernière étape, l'enregistrement de votre "Master Key". Vous devrez accéder à la page paramètre puis insérer votre token préalablement générer sur la plateforme Web et le valider en cliquant sur le bouton.

.. image:: _static/images/excel_config_token.png
   :align: center

Votre App-in est maintenant opérationnel.  

***********
Utilisation
***********

Présentation
============

Lorsque vous ouvrez l'add-in, vous arrivez à la page d'acceuil de celle-ci. Vous aurez face à vous 3 boutons, un pour prédire le résultat d'un banque de données, un autre pour créer un projet et un dernier pour parametrer votre add-in.

.. image:: _static/images/excel_home.png
   :align: center

Création d'un Projet
====================

.. image:: _static/images/excel_create.png
   :align: center

Pour créer un cas d'usage, vous devrez remplir les différents champs de texte, choisir votre problématique parmi "Segmentation" , "Classification" et "Multiclassification" puis le nom de votre Projet. Les autre paramètres sont prédéfinit :

   * Profil : Quick
   * Modèle : XBG et LR
   * Mélange de modèle : Oui
   * Transformations déselectionnées : Incorporation d'entités & ACP / K-means

Si l'un de ces paramètres ne correspond pas entièrement à vos attentes, vous pouvez toujours créer votre cas d'usage directement sur la plateforme web.

Réaliser une prédiction
=======================

.. image:: _static/images/excel_predict.png
   :align: center

Pour prédire de nouvelles données, vous devrez charger les noms des cas d'usages existants grace aux boutons joints avec le menu déroulant, puis remplir les champs de texte correspondant et lancer la prediction grace au bouton dédié.

Le resultat de la prédiction s'écrira automatiquement dans la colonne sortie précisé.

Paramètrer l'add-in
===================

Comme préciser dans l'installation, la fenêtre Paramètre sert à configurer votre add-in. Pour cela vous devrez insérer votre token qui sera enregistré et vous pourrez aussi enregistrer votre DNS personnel donnée par Prevision.

*********************
Astuce et Restriction
*********************

Le bouton joint avec les champs de texte pour la selection des plages de données sert à integrer directement l'adresse de la plage active.

Le traitement des plages de données ne supporte pas les plages multiples donc veillez à n'insérer qu'une seul plage sans colonne vide

L'add-in a besoin que les entêtes soit comprise dans les plages sélectionnées.

*********
Dépannage
*********

Si vous rencontrez une erreur, veuillez contacter notre support à l'adresse mail suivante : support@prevision.io