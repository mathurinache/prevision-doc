#########
Use cases
#########

*******************
Dashboard use cases
*******************

This dashboard allows you to see all usecases that you have created. You can access it by clicking on:

.. image:: _static/images/side_bar_dashboard.png
   :align: center

.. image:: _static/images/dashboard.png
   :align: center

You can create a new usecase by clicking on top right button or you can check all informations on previously done usecases in the table.
This table is sortable and filterable by usecase name / usecase type. Also, names can be clicked and will redirect you on the detailed view of the selected usecase.

On each line of the table, you can see:

* The usecase name
* The creation date
* The data type (among "tabular", "time series", "images")
* The training type (among "regression", "classification", "multiclassification", "object-detection")
* The actual score, the optimized metric and a star system allowing you to quickly know if a model is well performing
* The number of trained models
* The number of predictions done
* The number of people you have share the use case with (none by default)
* The status of the usecase (running, done, crashed)
* Actions linked to the usecase:

    * Pause or resume a running usecase
    * Share a use case
    * Make predictions
    * Stop or delete a usecase

Star system
===========

By default, when no model is available, 3 gray stars will be displayed (and 0 blue).
As soon as at least 1 model is available, the number of stars may change (up to 3 blue stars will be displayed according to the current performance of the modelisation).

The detail of the computation is explained here:

+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| METRIC         | 3 STARS                            | 2 STARS                                                          | 1 STAR                               |
+================+====================================+==================================================================+======================================+
| MSE            | [0 ; 0.01 * VAR[                   | [0.01 * VAR ; 0.1 * VAR[                                         | [0.1 * VAR ; VAR[                    |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| RMSE           | [0 ; 0.1 * STD[                    | [0.1 * STD ; 0.3 * STD[                                          | [0.3 * STD ; STD[                    |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MAE            | [0 ; 0.1 * STD[                    | [0.1 * STD ; 0.3 * STD[                                          | [0.3 * STD ; STD[                    |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MAPE           | [0 ; 10[                           | [10 ; 30[                                                        | [30 ; 1[                             |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| RMSLE          | [0 ; 0.095[                        | [0.095 ; 0.262[                                                  | [0.262 ; 1[                          |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| RMSPE          | [0 ; 0.1[                          | [0.1 ; 0.3[                                                      | [0.3 ; 1[                            |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| SMAPE          | [0 ; 0.1[                          | [0.1 ; 0.3[                                                      | [0.3 ; 1[                            |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MER            | [0 ; 0.1[                          | [0.1 ; 0.3[                                                      | [0.3 ; 1[                            |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| R2             | ]0.9 ; 1]                          | ]0.7 ; 0.9]                                                      | ]0.5 ; 0.7]                          |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| AUC            | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| LOGLOSS        | [0 ; 0.223[                        | [0.223 ; 0.693[                                                  | [0.693 ; +inf[                       |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| ERROR RATE     | [0 ; 0.125[                        | [0.125 ; 0.25[                                                   | [0.25 ; +inf[                        |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| mAP            | [0 ; 45[                           | [45 ; 60[                                                        | [60 ; 100[                           |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| AUCPR          | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| ACCURACY       | ]0.875 ; 1]                        | ]0.75 ; 0.875]                                                   | ]0.5 ; 0.75]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| F1             | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MCC            | ]0.9 ; 1]                          | ]0.7 ; 0.9]                                                      | ]0.5 ; 0.7]                          |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| GINI           | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| F05            | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| F2             | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| F3             | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| F4             | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MACRO F1       | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MACRO AUC      | ]0.85 ; 1]                         | ]0.65 ; 0.85]                                                    | ]0.5 ; 0.65]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MACRO ACCURACY | ]0.875 ; 1]                        | ]0.75 ; 0.875]                                                   | ]0.5 ; 0.75]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| QUADRATIC KAPPA| ]0.8 ; 1]                          | ]0.6 ; 0.8]                                                      | ]0.2 ; 0.6]                          |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| MAP @ k        | ]0.875 ; 1]                        | ]0.75 ; 0.875]                                                   | ]0.5 ; 0.75]                         |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+
| LIFT @ k       | ]1 + 7 * (1-k) ; 1]                | ]1 + 3 * (1-k) ; 1 + 7 * (1-k)]                                  | ]1 + 1 * (1-k) ; 1 + 3 * (1-k)]      |
+----------------+------------------------------------+------------------------------------------------------------------+--------------------------------------+

VAR is the variance of the target feature
STD is the standard deviation of the target feature

Actions links
=============

Actions links are available when clicking the top right "..." icon on the usecase dashboard..

.. image:: _static/images/btn_action.png
   :align: center

.. image:: _static/images/btn_action_2.png
   :align: center

Actions will change depending of the usecase status (running, paused, finished).

Pausing a usecase
-----------------

As long as a use case is running, you can pause it by clicking on the "pause" action link.

The pause isn't immediate. All running calculus are beeing finished before the actual pause which can take a couple of time depending on the data set size.

To resume a usecase, just click on the "resume" action link.

Sharing a usecase
-----------------

You can share a usecase by clicking on the "share" action link.

By default, a usecase isn't shared.
If you want to share it, just add an email of someone that has an account on your instance. He'll see the use case in his shared dashboard.

Once shared, you can see who has access:

.. image:: _static/images/usecase_shared.png
   :align: center

On the main dashboard, there will be a "1" displayed in the "shared" column, telling you that the usecase is currently shared with 1 user.

You can also stop sharing a usecase. To do so, remove the specific by clicking on the "trash" icon next to the desired user.

Making predictions
------------------

You can access the prediction tab by clicking on the "prediction" action link.

Stop the calculation
--------------------

In the case where a usecase is running it is possible to stop the calculation by clicking on the "stop" action link.

Contrary to the pause, this action is final, and the calculation cannot be restarted.

Delete a usecase
================

In case a usecase is finished, it is possible to delete it by clicking on "remove" action link.

This action is irreversible.